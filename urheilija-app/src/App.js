import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Urheilijatiedot from "./components/Urheilijatiedot";
import Ylatunniste from "./components/Ylatunniste";
import LisaaUrheilijatieto from "./components/LisaaUrheilijatieto";
import MuokkaaUrheilijatieto from "./components/MuokkaaUrheilijatieto";
import "bootstrap/dist/css/bootstrap.css";

import GlobalState from "./context/GlobalState";

const App = () => {
  return (
    <GlobalState>
      <Router>
        <div className="App">
          <Ylatunniste />
          <div className="container">
            <Routes>
              <Route path="/" element={<Urheilijatiedot />} />
              <Route
                path="/urheilija/lisaa"
                element={<LisaaUrheilijatieto />}
              />
              <Route
                path="/urheilija/muokkaa/:id"
                element={<MuokkaaUrheilijatieto />}
              />
            </Routes>
          </div>
        </div>
      </Router>
    </GlobalState>
  );
};

export default App;
