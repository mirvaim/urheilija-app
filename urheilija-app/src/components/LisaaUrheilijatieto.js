import React from "react";
import { useNavigate, Link } from "react-router-dom";
import { useState, useContext } from "react";
import urheilijatContext from "../context/UrheilijatContext";

export default function LisaaUrheilijatieto() {
  let history = useNavigate();
  const [etunimi, setEtunimi] = useState("");
  const [sukunimi, setSukunimi] = useState("");
  const [kutsumanimi, setKutsumanimi] = useState("");
  const [syntVuosi, setSyntVuosi] = useState("");
  const [paino, setPaino] = useState("");
  const [kuvalinkki, setKuvalinkki] = useState("");
  const [laji, setLaji] = useState("");
  const [saavutukset, setSaavutukset] = useState("");
  const UrheilijatContext = useContext(urheilijatContext);

  const handleSubmit = async (e) => {
    const uusiUrheilijatieto = {
      etunimi: etunimi,
      sukunimi: sukunimi,
      kutsumanimi: kutsumanimi,
      syntVuosi: syntVuosi,
      paino: paino,
      kuvalinkki: kuvalinkki,
      laji: laji,
      saavutukset: saavutukset,
    };
    console.log("Tarkistetaan uusiUrheilijatieto-objekti:");
    console.log(uusiUrheilijatieto);

    UrheilijatContext.setUusiUrheilija(uusiUrheilijatieto);
    history("/");
  };
  return (
    <>
      <h2 className="display-6 mb-4">
        <span className="text-dark">Lisää urheilija</span>
      </h2>

      <div className="container-sm justify-content-center">
        <div className="card-body">
          <form onSubmit={handleSubmit.bind(this)}>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Etunimi:</label>
              <input
                id="etunimitieto"
                type="text"
                name="etunimi"
                className="form-control form-control-lg"
                placeholder="Syötä etunimi..."
                value={etunimi}
                onChange={(event) => setEtunimi(event.target.value)}
              />
              <div className="invalid-feedback">Täytä etunimi</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="sukunimi">Sukunimi:</label>
              <input
                id="sukunimitieto"
                type="text"
                name="sukunimi"
                className="form-control form-control-lg"
                placeholder="Syötä sukunimi..."
                value={sukunimi}
                onChange={(event) => setSukunimi(event.target.value)}
              />
              <div className="invalid-feedback">Täytä sukunimi</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="kutsumanimi">Kutsumanimi:</label>
              <input
                id="kutsumanimitieto"
                type="text"
                name="kutsumanimi"
                className="form-control form-control-lg"
                placeholder="Syötä kutsumanimi..."
                value={kutsumanimi}
                onChange={(event) => setKutsumanimi(event.target.value)}
              />
              <div className="invalid-feedback">Täytä kutsumanimi</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="syntVuosi">Syntymävuosi:</label>
              <input
                id="syntVuositieto"
                type="text"
                name="syntVuosi"
                className="form-control form-control-lg"
                placeholder="Syötä syntymavuosi..."
                value={syntVuosi}
                onChange={(event) => setSyntVuosi(event.target.value)}
              />
              <div className="invalid-feedback">Täytä syntymävuosi</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="paino">Paino:</label>
              <input
                id="painotieto"
                type="text"
                name="paino"
                className="form-control form-control-lg"
                placeholder="Syötä paino..."
                value={paino}
                onChange={(event) => setPaino(event.target.value)}
              />
              <div className="invalid-feedback">Täytä paino</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="kuvalinkki">Kuvalinkki:</label>
              <input
                id="kuvalinkkitieto"
                type="text"
                name="kuvalinkki"
                className="form-control form-control-lg"
                placeholder="Syötä kuvalinkki..."
                value={kuvalinkki}
                onChange={(event) => setKuvalinkki(event.target.value)}
              />
              <div className="invalid-feedback">Täytä kuvalinkki</div>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="laji">Laji:</label>
              <input
                id="lajitieto"
                type="text"
                name="laji"
                className="form-control form-control-lg"
                placeholder="Syötä laji..."
                value={laji}
                onChange={(event) => setLaji(event.target.value)}
              />
              <div className="invalid-feedback">Täytä laji</div>
            </div>
            <div className="form-group">
              <label htmlFor="sukunimi">Saavutukset:</label>
              <input
                id="saavutuksettieto"
                type="text"
                name="saavutukset"
                className="form-control form-control-lg"
                placeholder="Syötä viimeisimmät saavutukset..."
                value={saavutukset}
                onChange={(event) => setSaavutukset(event.target.value)}
              />
              <div className="invalid-feedback">Täytä saavutukset</div>
            </div>
            <div
              class="btn-toolbar"
              role="toolbar"
              aria-label="Toolbar with button groups"
            >
              <div class="btn-group mt-3" role="group" aria-label="First group">
                <div class="mt-2">
                  <input type="submit" value="Lisää" class="btn btn-primary" />
                </div>
              </div>
              <div
                class="btn-group mt-3"
                role="group"
                aria-label="Second group"
              >
                <div class="m-2">
                  <Link to={`/`}>
                    <button class="btn btn-danger">Peruuta</button>
                  </Link>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
