import React, { useEffect, useState, useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import urheilijatContext from "../context/UrheilijatContext";

const MuokkaaUrheilijatieto = () => {
  const [etunimi, setEtunimi] = useState("");
  const [sukunimi, setSukunimi] = useState("");
  const [kutsumanimi, setKutsumanimi] = useState("");
  const [syntVuosi, setSyntVuosi] = useState("");
  const [paino, setPaino] = useState("");
  const [kuvalinkki, setKuvalinkki] = useState("");
  const [laji, setLaji] = useState("");
  const [saavutukset, setSaavutukset] = useState("");
  const [list, setList] = useState([]);
  const UrheilijatContext = useContext(urheilijatContext);
  const { id } = useParams();

  let history = useNavigate();

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      const urheilija = UrheilijatContext.getUrheilija(id).then((res) => {
        setEtunimi(res.etunimi);
        setSukunimi(res.sukunimi);
        setKutsumanimi(res.kutsumanimi);
        setSyntVuosi(res.syntVuosi);
        setPaino(res.paino);
        setKuvalinkki(res.kuvalinkki);
        setLaji(res.laji);
        setSaavutukset(res.saavutukset);
      });
    } else mounted = false;
  }, []);

  const handleSubmit = async (e) => {
    const paivitettyUrheilijatieto = {
      etunimi: etunimi,
      sukunimi: sukunimi,
      kutsumanimi: kutsumanimi,
      syntVuosi: syntVuosi,
      paino: paino,
      kuvalinkki: kuvalinkki,
      laji: laji,
      saavutukset: saavutukset,
    };

    UrheilijatContext.setUrheilija(id, paivitettyUrheilijatieto);
    history("/");
  };
  const onChangeEtunimi = (e) => {
    setEtunimi(e.target.value);
  };
  const onChangeSukunimi = (e) => {
    setSukunimi(e.target.value);
  };
  const onChangeKutsumanimi = (e) => {
    setKutsumanimi(e.target.value);
  };
  const onChangeSyntVuosi = (e) => {
    setSyntVuosi(e.target.value);
  };
  const onChangePaino = (e) => {
    setPaino(e.target.value);
  };
  const onChangeKuvalinkki = (e) => {
    setKuvalinkki(e.target.value);
  };
  const onChangeLaji = (e) => {
    setLaji(e.target.value);
  };
  const onChangeSaavutukset = (e) => {
    setSaavutukset(e.target.value);
  };
  return (
    <>
      <h2 className="display-6 mb-4">
        <span className="text-dark">Muokkaa urheilijan tietoja</span>
      </h2>

      <div className="container-sm justify-content-center">
        <div className="card-body">
          <form onSubmit={handleSubmit.bind(this)}>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Etunimi:</label>
              <input
                type="text"
                name="etunimi"
                className="form-control form-control-lg"
                placeholder="Syötä etunimi..."
                value={etunimi}
                onChange={onChangeEtunimi}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Sukunimi:</label>
              <input
                type="text"
                name="sukunimi"
                className="form-control form-control-lg"
                placeholder="Syötä sukunimi..."
                value={sukunimi}
                onChange={onChangeSukunimi}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Kutsumanimi:</label>
              <input
                type="text"
                name="kutsumanimi"
                className="form-control form-control-lg"
                placeholder="Syötä kutsumanimi..."
                value={kutsumanimi}
                onChange={onChangeKutsumanimi}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Syntymävuosi:</label>
              <input
                type="text"
                name="syntVuosi"
                className="form-control form-control-lg"
                placeholder="Syötä syntymävuosi..."
                value={syntVuosi}
                onChange={onChangeSyntVuosi}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Paino:</label>
              <input
                type="text"
                name="paino"
                className="form-control form-control-lg"
                placeholder="Syötä paino..."
                value={paino}
                onChange={onChangePaino}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Kuvalinkki:</label>
              <input
                type="text"
                name="kuvalinkki"
                className="form-control form-control-lg"
                placeholder="Syötä kuvalinkki..."
                value={kuvalinkki}
                onChange={onChangeKuvalinkki}
              />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="nimi">Laji:</label>
              <input
                type="text"
                name="laji"
                className="form-control form-control-lg"
                placeholder="Syötä laji..."
                value={laji}
                onChange={onChangeLaji}
              />
            </div>
            <div className="form-group">
              <label htmlFor="nimi">Saavutukset:</label>
              <input
                type="text"
                name="saavutukset"
                className="form-control form-control-lg"
                placeholder="Syötä viimeisimmät saavutukset..."
                value={saavutukset}
                onChange={onChangeSaavutukset}
              />
            </div>
            <div
              class="btn-toolbar"
              role="toolbar"
              aria-label="Toolbar with button groups"
            >
              <div class="btn-group mt-3" role="group" aria-label="First group">
                <div class="mt-2">
                  <input
                    type="submit"
                    value="Tallenna"
                    class="btn btn-primary"
                  />
                </div>
              </div>
              <div
                class="btn-group mt-3"
                role="group"
                aria-label="Second group"
              >
                <div class="m-2">
                  <Link to={`/`}>
                    <button class="btn btn-danger">Peruuta</button>
                  </Link>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
export default MuokkaaUrheilijatieto;
