import React, { useContext, useEffect } from "react";
import Urheilijatieto from "./Urheilijatieto";
import urheilijatContext from "../context/UrheilijatContext";

const Urheilijatiedot = () => {
  const UrheilijatContext = useContext(urheilijatContext);
  console.log(UrheilijatContext);

  useEffect(() => {
    UrheilijatContext.getKaikkiUrheilijat(); //haetaan urheilijatiedot
    console.log(UrheilijatContext);
  }, [UrheilijatContext]); //kun komponentti piirretään, suoritetaan kerran
  return (
    <>
      <h1 className="display-5 mb-4">
        <span className="text-dark">Urheilijat</span>
      </h1>

      <div className="container-fluid d-flex flex-wrap justify-content-center">
        <React.Fragment>
          {UrheilijatContext.urheilijat.length
            ? UrheilijatContext.urheilijat.map((urheilija) => (
                <Urheilijatieto key={urheilija.id} urheilija={urheilija} />
              ))
            : null}
        </React.Fragment>
      </div>
    </>
  );
};
export default Urheilijatiedot;
