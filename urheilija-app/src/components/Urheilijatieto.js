import React from "react";
import { Link } from "react-router-dom";

import { useState, useContext } from "react";
import urheilijatContext from "../context/UrheilijatContext";
import { useNavigate } from "react-router-dom";
import "./urheilijakortti-style.css";

const Urheilijatieto = (props) => {
  const UrheilijatContext = useContext(urheilijatContext);
  let history = useNavigate();
  const onDeleteClick = (id) => {
    UrheilijatContext.poistaUrheilija(id);
    window.location.reload();
    history("/");
  };
  const {
    id,
    etunimi,
    sukunimi,
    kutsumanimi,
    syntVuosi,
    paino,
    kuvalinkki,
    laji,
    saavutukset,
  } = props.urheilija;
  return (
    <div className="m-2">
      <div className="card text-center shadow">
        <div className="overflow">
          <img src={kuvalinkki} className="card-img-top"></img>{" "}
        </div>
        <div className="card-body text-dark">
          <h4 className="card-title">{etunimi + " " + sukunimi}</h4>
          <p className="card-text text-dark">
            <b>Kutsumanimi:</b> {kutsumanimi}
            <br></br>
            <b>Syntymävuosi:</b> {syntVuosi}
            <br></br>
            <b>Paino:</b> {paino}
            <br></br>
            <b>Laji:</b> {laji}
            <br></br>
            <b>Saavutukset:</b> {saavutukset}
          </p>
          <div class="btn-group mt-3" role="group" aria-label="First group">
            <Link to={`urheilija/muokkaa/${id}`}>
              <button class="btn btn-warning">Muokkaa</button>
            </Link>
          </div>
          <div class="btn-group mt-3" role="group" aria-label="Second group">
            <div class="m-2">
              <button
                class="btn btn-danger"
                onClick={onDeleteClick.bind(this, { id })}
              >
                Poista
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Urheilijatieto;
