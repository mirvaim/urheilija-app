import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Ylatunniste = (props) => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4 py-0">
      <div className="container">
        <div>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                <i className="fas fa-home" />
                Etusivu
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/urheilija/lisaa" className="nav-link">
                Lisää urheilija
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Ylatunniste;
