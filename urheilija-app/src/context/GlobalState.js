import React, { useReducer } from "react";
import AppReducer from "./AppReducer";
import UrheilijatContext from "./UrheilijatContext";
import { GET_URHEILIJAT } from "./types";
import axios from "axios";
const GlobalState = (props) => {
  let initialState = {
    urheilijat: [],
  };
  //hae kaikki urheilijat
  const [state, dispatch] = useReducer(AppReducer, initialState);
  const getKaikkiUrheilijat = async () => {
    try {
      let res = await axios.get("");
      let { data } = res;
      dispatch({ type: GET_URHEILIJAT, payload: data });
    } catch (error) {
      console.error(error);
    }
  };
  //hae urheilija
  const getUrheilija = async (id) => {
    try {
      let sql = "" + id;
      let res = await axios.get(sql);
      let { data } = res;
      console.log("GET_URHEILIJA:");
      dispatch({ type: "GET_URHEILIJA", payload: data });
      return data;
    } catch (error) {
      console.error(error);
    }
  };
  //lisaa urheilija
  const setUusiUrheilija = async (uusiUrheilijatieto) => {
    try {
      const res = await axios.post(``, uusiUrheilijatieto).then((res) => {
        dispatch({ type: "ADD_URHEILIJA", payload: res.data });
        console.log(res.data);
      });
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  };
  //muokkaa urheilijaa
  const setUrheilija = async (id, paivitettyUrheilijatieto) => {
    try {
      const res = await axios.put(``, paivitettyUrheilijatieto).then((res) => {
        dispatch({ type: "EDIT_URHEILIJA", payload: res.data });
        console.log(res.data);
      });
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  };
  //poista urheilija
  const poistaUrheilija = async (id) => {
    try {
      let sql = "";

      const res = await axios.delete(sql).then((res) => {
        dispatch({ type: "DELETE_URHEILIJA", payload: id["id"] });
        console.log(res.data);
      });
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <UrheilijatContext.Provider
      value={{
        urheilijat: state.urheilijat,
        getKaikkiUrheilijat,
        setUusiUrheilija,
        setUrheilija,
        poistaUrheilija,
        getUrheilija,
      }}
    >
      {props.children}
    </UrheilijatContext.Provider>
  );
};

export default GlobalState;
